Name: bubblewrap-ln-flatpak
Version: 1
Release: 1
Summary: use flatpak-bwrap instead bwrap (for CentOS 7)
BuildArch: noarch
License: None
Requires: flatpak
Provides: bubblewrap
%description
use flatpak-bwrap instead bwrap (for CentOS 7)

%install
mkdir %{buildroot}/usr
mkdir %{buildroot}/usr/bin
ln -s /usr/libexec/flatpak-bwrap %{buildroot}/usr/bin/bwrap

%files
/usr/bin/bwrap


