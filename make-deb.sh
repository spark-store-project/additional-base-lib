#!/bin/bash

LIBC6_DOWNLOAD_URL=http://mirrors.ustc.edu.cn/debian/pool/main/g/glibc/libc6_2.38-13_amd64.deb
LIBC_BIN_DOWNLOAD_URL=http://mirrors.ustc.edu.cn/debian/pool/main/g/glibc/libc-bin_2.38-13_amd64.deb
LIBSTDCxx6_DOWNLOAD_URL=http://mirrors.ustc.edu.cn/debian/pool/main/g/gcc-14/libstdc++6_14-20240330-1_amd64.deb

ABL_VERSION=9

check(){
    if [ "$1" != 0 ]
    then
        echo "Error"
        exit
    fi
}


echo "download packages:"
mkdir ./downloads
echo "download libc6..."
wget "$LIBC6_DOWNLOAD_URL" -c -O ./downloads/libc6.deb
check $?
echo "download libc-bin..."
wget "$LIBC_BIN_DOWNLOAD_URL" -c -O ./downloads/libc-bin.deb
check $?
echo "download libstdc++6..."
wget "$LIBSTDCxx6_DOWNLOAD_URL" -c -O ./downloads/libstdc++6.deb
check $?

echo
echo "extract packages:"
echo "extract libc6..."
dpkg -x ./downloads/libc6.deb ./downloads/libc6
check $?
dpkg -e ./downloads/libc6.deb ./downloads/libc6/DEBIAN
check $?
echo "extract libc-bin..."
dpkg -x ./downloads/libc-bin.deb ./downloads/libc-bin
check $?
dpkg -e ./downloads/libc-bin.deb ./downloads/libc-bin/DEBIAN
check $?
echo "extract libstdc++6..."
dpkg -x ./downloads/libstdc++6.deb ./downloads/libstdc++6
check $?
dpkg -e ./downloads/libstdc++6.deb ./downloads/libstdc++6/DEBIAN
check $?

echo
echo "check packages:"
echo "check version libc6==libc-bin"
LIBC6_VERSION=`cat ./downloads/libc6/DEBIAN/control | grep "^Version: .*"`
check $?
LIBC6_VERSION=${LIBC6_VERSION:9}
LIBC_BIN_VERSION=`cat ./downloads/libc-bin/DEBIAN/control | grep "^Version: .*"`
check $?
LIBC_BIN_VERSION=${LIBC_BIN_VERSION:9}
LIBSTDCxx6_VERSION=`cat ./downloads/libstdc++6/DEBIAN/control | grep "^Version: .*"`
check $?
LIBSTDCxx6_VERSION=${LIBSTDCxx6_VERSION:9}

echo "$LIBC6_VERSION==$LIBC_BIN_VERSION"
[ "$LIBC6_VERSION" = "$LIBC_BIN_VERSION" ]
check $?

echo "check arch libc6==libc-bin"
LIBC6_ARCH=`cat ./downloads/libc6/DEBIAN/control | grep "^Architecture: .*"`
check $?
LIBC6_ARCH=${LIBC6_ARCH:14}
LIBC_BIN_ARCH=`cat ./downloads/libc-bin/DEBIAN/control | grep "^Architecture: .*"`
check $?
LIBC_BIN_ARCH=${LIBC_BIN_ARCH:14}

echo "$LIBC6_ARCH==$LIBC_BIN_ARCH"
[ "$LIBC6_ARCH" = "$LIBC_BIN_ARCH" ]
check $?

echo "check arch libc6==libstdc++6"
LIBSTDCxx6_ARCH=`cat ./downloads/libstdc++6/DEBIAN/control | grep "^Architecture: .*"`
check $?
LIBSTDCxx6_ARCH=${LIBSTDCxx6_ARCH:14}

echo "$LIBC6_ARCH==$LIBSTDCxx6_ARCH"
[ "$LIBC6_ARCH" = "$LIBSTDCxx6_ARCH" ]
check $?

echo
echo "collect information:"
echo "glibc version:"
GLIBC_VERSION=$LIBC6_VERSION
echo "$GLIBC_VERSION"

echo "deb package architecture:"
DEBIAN_DEB_ARCH=$LIBC6_ARCH
echo "$DEBIAN_DEB_ARCH"

echo "debian multiarch directory name:"
DEBIAN_MULTIARCH=`dpkg-architecture -A "$DEBIAN_DEB_ARCH" | grep "^DEB_TARGET_MULTIARCH=.*"`
check $?
DEBIAN_MULTIARCH=${DEBIAN_MULTIARCH:21}
echo "$DEBIAN_MULTIARCH"

echo "check usrmerge status:":
if [ -d "./downloads/libc6/lib" ]
then
  LIBC_LIB_DIR=""
  echo "not usrmerge"
else
  LIBC_LIB_DIR="usr/"
  echo "usrmerge"
fi

echo "ld.so location:"
LD_SO_LOCATION=`patchelf --print-interpreter "./downloads/libc6/${LIBC_LIB_DIR}lib/${DEBIAN_MULTIARCH}/libc.so.6"`
check $?
echo "$LD_SO_LOCATION"

echo
echo "build package directories..."
mkdir ./deb-contents
mkdir ./deb-contents/DEBIAN
mkdir ./deb-contents/usr
mkdir ./deb-contents/usr/bin
mkdir ./deb-contents/usr/lib
mkdir "./deb-contents/usr/lib/$DEBIAN_MULTIARCH"

echo
echo "gather files:"

echo "generate ablrun script..."
echo "#!/bin/bash" > ./deb-contents/usr/bin/ablrun
echo "ABL_TARGET_LD_SO_PATH=$LD_SO_LOCATION" >> ./deb-contents/usr/bin/ablrun
echo "ABL_DIR_PREFIX=lib/$DEBIAN_MULTIARCH" >> ./deb-contents/usr/bin/ablrun
cat ./scripts/ablrun_part >> ./deb-contents/usr/bin/ablrun
check $?

echo "chmod..."
chmod a+x ./deb-contents/usr/bin/*
check $?

rooted_readlink(){
    rrl_result=$1
    rrl_root=$2
    while [ -h "$rrl_result"  ]
    do
        rrl_result=`readlink "$rrl_result"`
        if [ "${rrl_result:0:1}" == "/" ]
        then
            rrl_result="$rrl_root/$rrl_result"
        else
            rrl_result=`dirname "$1"`"/$rrl_result"
        fi
    done
    echo -n "$rrl_result"
}

echo "copy many libc6 components..."
cp -r "./downloads/libc6/${LIBC_LIB_DIR}lib/${DEBIAN_MULTIARCH}/" "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib"
check $?

echo "copy ld.so..."
mkdir --parents "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib/$LD_SO_LOCATION"
rm -d "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib/$LD_SO_LOCATION"
cp `rooted_readlink "./downloads/libc6/${LIBC_LIB_DIR}${LD_SO_LOCATION}" "./downloads/libc6"` "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib/$LD_SO_LOCATION"
check $?

echo "copy ldd script..."
cp "./downloads/libc-bin/usr/bin/ldd" "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib/ldd"
check $?

echo "copy libstdc++..."
cp `rooted_readlink ./downloads/libstdc++6/usr/lib/${DEBIAN_MULTIARCH}/libstdc++.so.6 ./downloads/libstdc++6/` "./deb-contents/usr/lib/${DEBIAN_MULTIARCH}/additional-base-lib/libstdc++.so.6"
check $?

echo "calculate file size..."
DEB_INSTALL_SIZE=(`du -s ./deb-contents/`)

echo
echo "create control file..."
echo "Package: additional-base-lib" >> ./deb-contents/DEBIAN/control
echo "Version: $GLIBC_VERSION-$ABL_VERSION" >> ./deb-contents/DEBIAN/control
echo "Section: utils" >> ./deb-contents/DEBIAN/control
echo "Priority: optional" >> ./deb-contents/DEBIAN/control
echo "Installed-Size: ${DEB_INSTALL_SIZE[@]:0:1}" >> ./deb-contents/DEBIAN/control
echo "Architecture: $DEBIAN_DEB_ARCH" >> ./deb-contents/DEBIAN/control
echo "Maintainer: CongTianKong" >> ./deb-contents/DEBIAN/control
echo "Depends: bubblewrap, bash, coreutils" >> ./deb-contents/DEBIAN/control
echo "Suggests: shared-mime-info, xdg-utils" >> ./deb-contents/DEBIAN/control
echo "Homepage: https://gitee.com/deepin-community-store/additional-base-lib" >> ./deb-contents/DEBIAN/control
echo "Description: A script to run programs with newer libc." >> ./deb-contents/DEBIAN/control
echo " package built with make-deb.sh from additional-base-lib project." >> ./deb-contents/DEBIAN/control
echo " libc6 download URL: $LIBC6_DOWNLOAD_URL" >> ./deb-contents/DEBIAN/control
echo " libc-bin download URL: $LIBC_BIN_DOWNLOAD_URL" >> ./deb-contents/DEBIAN/control
echo " libstdc++6 download URL: $LIBSTDCxx6_DOWNLOAD_URL" >> ./deb-contents/DEBIAN/control

echo >> ./deb-contents/DEBIAN/control
check $?

echo
echo "build deb package:"
dpkg-deb -Zgzip -b ./deb-contents "./additional-base-lib_${GLIBC_VERSION}-${ABL_VERSION}_${DEBIAN_DEB_ARCH}.deb"
# use gzip to compress for compatibility
check $?

echo
echo "complete with no error!"