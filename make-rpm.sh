#!/bin/bash

GLIBC_DOWNLOAD_URL=https://kojipkgs.fedoraproject.org//packages/glibc/2.39/8.fc40/x86_64/glibc-2.39-8.fc40.x86_64.rpm
GLIBC_COMMON_DOWNLOAD_URL=https://kojipkgs.fedoraproject.org//packages/glibc/2.39/8.fc40/x86_64/glibc-common-2.39-8.fc40.x86_64.rpm
LIBSTDCxx_DOWNLOAD_URL=https://kojipkgs.fedoraproject.org//packages/gcc/14.0.1/0.15.fc40/x86_64/libstdc++-14.0.1-0.15.fc40.x86_64.rpm

ABL_VERSION=9
ABL_RPM_RELEASE=1

check(){
    if [ "$1" != 0 ]
    then
        echo "Error"
        exit
    fi
}

echo "download packages:"
mkdir ./downloads
echo "download glibc..."
wget "$GLIBC_DOWNLOAD_URL" -c -O ./downloads/glibc.rpm
check $?
echo "download glibc-common..."
wget "$GLIBC_COMMON_DOWNLOAD_URL" -c -O ./downloads/glibc-common.rpm
check $?
echo "download libstdc++..."
wget "$LIBSTDCxx_DOWNLOAD_URL" -c -O ./downloads/libstdc++.rpm

echo
echo "check packages:"
echo "check version glibc==glibc-common"
GLIBC_VERSION=`rpm -q ./downloads/glibc.rpm --qf %{VERSION}`
check $?
GLIBC_COMMON_VERSION=`rpm -q ./downloads/glibc-common.rpm --qf %{VERSION}`
check $?
LIBSTDCxx_VERSION=`rpm -q ./downloads/libstdc++.rpm --qf %{VERSION}`
check $?

echo "$GLIBC_VERSION==$GLIBC_COMMON_VERSION"
[ "$GLIBC_VERSION" = "$GLIBC_COMMON_VERSION" ]
check $?

echo "check release glibc==glibc-common"
GLIBC_RELEASE=`rpm -q ./downloads/glibc.rpm --qf %{RELEASE}`
check $?
GLIBC_COMMON_RELEASE=`rpm -q ./downloads/glibc-common.rpm --qf %{RELEASE}`
check $?
LIBSTDCxx_RELEASE=`rpm -q ./downloads/libstdc++.rpm --qf %{RELEASE}`
check $?

echo "$GLIBC_RELEASE==$GLIBC_COMMON_RELEASE"
[ "$GLIBC_RELEASE" = "$GLIBC_COMMON_RELEASE" ]
check $?

echo "check arch glibc==glibc-common"
GLIBC_ARCH=`rpm -q ./downloads/glibc.rpm --qf %{ARCH}`
check $?
GLIBC_COMMON_ARCH=`rpm -q ./downloads/glibc-common.rpm --qf %{ARCH}`
check $?

echo "$GLIBC_ARCH==$GLIBC_COMMON_ARCH"
[ "$GLIBC_ARCH" = "$GLIBC_COMMON_ARCH" ]
check $?

echo "check arch glibc==libstdc++"
LIBSTDCxx_ARCH=`rpm -q ./downloads/libstdc++.rpm --qf %{ARCH}`
check $?

echo "$GLIBC_ARCH==$LIBSTDCxx_ARCH"
[ "$GLIBC_ARCH" = "$LIBSTDCxx_ARCH" ]
check $?

echo
echo "prepare rpmbuild tree..."
mkdir ~/rpmbuild
mkdir ~/rpmbuild/BUILD
mkdir ~/rpmbuild/RPMS
mkdir ~/rpmbuild/SPECS

echo
echo "extract files..."

echo "extract glibc.rpm"
mkdir ~/rpmbuild/BUILD/glibc
check $?
rpm2cpio ./downloads/glibc.rpm | cpio -idmvD ~/rpmbuild/BUILD/glibc
check $?

echo "extract glibc-common.rpm"
mkdir ~/rpmbuild/BUILD/glibc-common
check $?
rpm2cpio ./downloads/glibc-common.rpm | cpio -idmvD ~/rpmbuild/BUILD/glibc-common
check $?

echo "extract libstdc++.rpm"
mkdir ~/rpmbuild/BUILD/libstdc++
check $?
rpm2cpio ./downloads/libstdc++.rpm | cpio -idmvD ~/rpmbuild/BUILD/libstdc++
check $?

echo
echo "collect information:"
echo "glibc version"
echo "$GLIBC_VERSION"

echo "glibc release"
echo "$GLIBC_RELEASE"

echo "rpm architecture:"
echo "$GLIBC_ARCH"

echo "library dir:"
if [ -d ~/rpmbuild/BUILD/glibc/lib64 ]
then
    SYSTEM_LIBRARY_DIR=lib64
else
    [ -d ~/rpmbuild/BUILD/glibc/lib ]
    check $?
    SYSTEM_LIBRARY_DIR=lib
fi
echo "$SYSTEM_LIBRARY_DIR"

echo "ld.so location:"
LD_SO_LOCATION=`patchelf --print-interpreter ~/rpmbuild/BUILD/glibc/$SYSTEM_LIBRARY_DIR/libc.so.6`
check $?
echo "$LD_SO_LOCATION"

echo
echo "build rpm spec file..."
echo "%global __brp_strip %{nil}" > ~/rpmbuild/SPECS/additional-base-lib.spec
# disable strip, for it may fail when packaging for another architecture.
echo "%global _binary_payload w9.gzdio" >> ~/rpmbuild/SPECS/additional-base-lib.spec
# use gzip to compress for compatibility.
echo "Name: additional-base-lib" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "Version: $GLIBC_VERSION.$GLIBC_RELEASE.$ABL_VERSION" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "Release: $ABL_RPM_RELEASE" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "Summary: A script to run programs with newer libc." >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "License: Mix" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "URL: https://gitee.com/deepin-community-store/additional-base-lib" >> ~/rpmbuild/SPECS/additional-base-lib.spec
#echo "BuildArch: $GLIBC_ARCH" >> ~/rpmbuild/SPECS/additional-base-lib.spec
# we don't use this field, pass --target to rpmbuild is enough.
echo "Requires: bubblewrap, bash, coreutils" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "Suggests: shared-mime-info, xdg-utils" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "%install" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "cp -r ./abl-rpm/* %{buildroot}" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "%description" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "A script to run programs with newer libc." >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "package built with make-rpm.sh from additional-base-lib project." >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "glibc download URL: $GLIBC_DOWNLOAD_URL" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "glibc-common download URL: $GLIBC_COMMON_DOWNLOAD_URL" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "libstdc++ download URL: $LIBSTDCxx_DOWNLOAD_URL" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "%files" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "%dir /usr/$SYSTEM_LIBRARY_DIR/additional-base-lib" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "/usr/$SYSTEM_LIBRARY_DIR/additional-base-lib/*" >> ~/rpmbuild/SPECS/additional-base-lib.spec
echo "/usr/bin/ablrun" >> ~/rpmbuild/SPECS/additional-base-lib.spec
check $?

echo
echo "build package directories..."
mkdir ~/rpmbuild/BUILD/abl-rpm
mkdir ~/rpmbuild/BUILD/abl-rpm/usr
mkdir ~/rpmbuild/BUILD/abl-rpm/usr/$SYSTEM_LIBRARY_DIR
mkdir ~/rpmbuild/BUILD/abl-rpm/usr/bin

echo
echo "gather files:"
echo "generate ablrun script..."
echo "#!/bin/bash" > ~/rpmbuild/BUILD/abl-rpm/usr/bin/ablrun
echo "ABL_TARGET_LD_SO_PATH=$LD_SO_LOCATION" >> ~/rpmbuild/BUILD/abl-rpm/usr/bin/ablrun
echo "ABL_DIR_PREFIX=$SYSTEM_LIBRARY_DIR" >> ~/rpmbuild/BUILD/abl-rpm/usr/bin/ablrun
cat ./scripts/ablrun_part >> ~/rpmbuild/BUILD/abl-rpm/usr/bin/ablrun
check $?

echo "chmod..."
chmod a+x ~/rpmbuild/BUILD/abl-rpm/usr/bin/ablrun
check $?

rooted_readlink(){
    rrl_result=$1
    rrl_root=$2
    while [ -h "$rrl_result"  ]
    do
        rrl_result=`readlink "$rrl_result"`
        if [ "${rrl_result:0:1}" == "/" ]
        then
            rrl_result="$rrl_root/$rrl_result"
        else
            rrl_result=`dirname "$1"`"/$rrl_result"
        fi
    done
    echo -n "$rrl_result"
}

echo "copy many glibc components..."
cp -r ~/"rpmbuild/BUILD/glibc/${SYSTEM_LIBRARY_DIR}/" ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib"
check $?

echo "copy ld.so..."
mkdir --parents ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib/$LD_SO_LOCATION"
rm -d ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib/$LD_SO_LOCATION"
cp `rooted_readlink ~/"rpmbuild/BUILD/glibc/$LD_SO_LOCATION" ~/"rpmbuild/libc6"` ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib/$LD_SO_LOCATION"
check $?

echo "copy ldd script..."
cp ~/"rpmbuild/BUILD/glibc-common/usr/bin/ldd" ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib/ldd"
check $?

echo "copy libstdc++"
cp `rooted_readlink ~/"rpmbuild/BUILD/libstdc++/usr/${SYSTEM_LIBRARY_DIR}/libstdc++.so.6" ~/"rpmbuild/BUILD/libstdc++"` ~/"rpmbuild/BUILD/abl-rpm/usr/${SYSTEM_LIBRARY_DIR}/additional-base-lib/libstdc++.so.6"
check $?

echo
echo "build rpm package:"
rpmbuild -bb ~/rpmbuild/SPECS/additional-base-lib.spec --target "$GLIBC_ARCH"
check $?

echo
echo "complete with no error!"
echo "RPM package may locate in ~/rpmbuild/RPMS"
echo "You may clean ~/rpmbuild/BUILD and ./downloads for a new start."

